postgresfixture (0.5.0-1) unstable; urgency=medium

  * New upstream release:
    - Drop support for Python 2; add up to 3.13 (closes: #1084648).
  * Enable autopkgtest-pkg-pybuild.

 -- Colin Watson <cjwatson@debian.org>  Mon, 07 Oct 2024 23:19:36 +0100

postgresfixture (0.4.3-3) unstable; urgency=medium

  * Use pybuild-plugin-pyproject.
  * Set Rules-Requires-Root: no.

 -- Colin Watson <cjwatson@debian.org>  Sat, 16 Mar 2024 01:20:46 +0000

postgresfixture (0.4.3-2) unstable; urgency=medium

  * Update watch file format version to 4.
  * Update standards version to 4.6.2, no changes needed.
  * Ignore changes to *.egg-info/* when building source package (closes:
    #1049150).

 -- Colin Watson <cjwatson@debian.org>  Mon, 14 Aug 2023 13:54:07 +0100

postgresfixture (0.4.3-1) unstable; urgency=medium

  * New upstream release:
    - Fix test failures if Cluster.running produces warnings (closes:
      #1020098).
    - Compare PostgreSQL versions using packaging.version.Version.

 -- Colin Watson <cjwatson@debian.org>  Tue, 27 Sep 2022 13:51:21 +0100

postgresfixture (0.4.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Colin Watson ]
  * Upgrade to debhelper v13.

 -- Colin Watson <cjwatson@debian.org>  Sun, 26 Dec 2021 00:50:21 +0000

postgresfixture (0.4.2-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 01 Oct 2019 13:24:10 +0100

postgresfixture (0.4.1-2) unstable; urgency=medium

  * Source-only upload to allow migration to testing.

 -- Colin Watson <cjwatson@debian.org>  Sun, 08 Sep 2019 17:20:41 +0100

postgresfixture (0.4.1-1) unstable; urgency=medium

  * Initial release (closes: #934596).

 -- Colin Watson <cjwatson@debian.org>  Mon, 12 Aug 2019 13:37:24 +0100
